var WebSocketServer = require('websocket').server;
var http = require('http');

var Tickers = require('./tickers.js');
Tickers.start();

var clients = [];

(function () {
  var ticks = Tickers.getAllTickers();
  var arr = Object.keys(ticks).map(function (k) { 
    return {
      market: k,
      price: ticks[k]
    };
  });
  for (var i = 0; i < clients.length; i++) {
    clients[i].sendUTF(JSON.stringify(arr));
  }
  // console.log(JSON.stringify(arr));
  setTimeout(arguments.callee, 5000);
})();

var server = http.createServer(function(request, response) {
  // process HTTP request. Since we're writing just WebSockets
  // server we don't have to implement anything.
});
server.listen(1337, function() { });

// create the server
wsServer = new WebSocketServer({
  httpServer: server
});

// WebSocket server
wsServer.on('request', function(request) {
  var connection = request.accept(null, request.origin);
  clients.push(connection);

  // This is the most important callback for us, we'll handle
  // all messages from users here.
  connection.on('message', function(message) {
    if (message.type === 'utf8') {
      // process WebSocket message
    }
  });

  connection.on('close', function(connection) {
    // close user connection
  });
});